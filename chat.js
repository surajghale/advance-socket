import React, { Component } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";
export default class serve extends Component {
  state = {
    image: null,
    message: "",
    Not_authorize:false
  };
  fileChangeHandler = (event) => {
    var fil = event.target.files[0];
    this.setState({ image: fil });
  };
  getBase64Image = (img) => {
    console.log("image is =============", img, img["name"]);
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  };
  sendMessage = () => {
    var token = localStorage.getItem("token")
    if(token){
      var username = localStorage.getItem("username");
      var ws = new WebSocket(`ws://localhost:8000/ws/${username}`);
      ws.onopen = () => {
        ws.send(JSON.stringify(this.state.message));
      };
      ws.onmessage = (event) => {
        // var returned_message = event.data
        var messages = document.getElementById("messages");
        var message = document.createElement("li");
        var content = document.createTextNode(event.data);
        message.appendChild(content);
        messages.appendChild(message);
      };
    }
    else{
      this.setState({"Not_authorize":true})
    }
    
  };
  handleSubmit = () => {
    var ws = new WebSocket("ws://localhost:8000/ws/{}");
    var reader = new FileReader();
    var data = null;
    reader.onloadend = function () {
      console.log("RESULT", reader.result);
      ws.onopen = () => {
        //send any msg from Client if needed
        ws.send(JSON.stringify(reader.result));
      };
      data = reader.result;
    };
    reader.readAsDataURL(this.state.image);
    console.log("data is +++++++++=", data);
    // var imageto_base64 = this.getBase64Image(this.state.unit_cost)

    // ws.send(this.state.unit_cost)
  };
  messageHandler = (e) => {
    console.log("form data is ", e.target.name);
    this.setState({ [e.target.name]: e.target.value });
  };
  leaveChat = (e)=>{
    e.preventDefault()
    var username = localStorage.getItem("username")
    var ws = new WebSocket(`ws://localhost:8000/ws/${username}`);
    ws.onopen = () => {
      ws.send(JSON.stringify("left"));
    };
    ws.onmessage = (event) => {
      // var returned_message = event.data
      
      document.getElementById("left").innerHTML = event.data
    };

  }
  componentDidMount() {}
  render() {
    return (
      <div>
        <input
          type="text"
          value={this.state.unit_cost}
          className="form-control"
          onChange={this.messageHandler}
          name="message"
          id="inputEmail3"
          placeholder="Unit Price"
        />
        <input
          type="file"
          onChange={this.fileChangeHandler}
          name="image"
          className="form-control"
          multiple
        />
        <button type="submit" onClick={this.sendMessage}>
          Button
        </button>
        <button type="submit" onClick={this.leaveChat}>
          Leave
        </button>
        <ul id="messages"></ul>
        <h1 id ="left"></h1>
        {this.state.Not_authorize?<NavLink to="/">authorize please</NavLink>:null}
      </div>
    );
  }
}
