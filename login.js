import React, { Component } from "react";
import * as actions from "../../store/actions/actions";
import { connect } from "react-redux";
import axios from "axios";
class Login extends Component {
  state = {
    formdata: {
      email: { value: "" },
      password: { value: "" },
    },
    loginStatus: "NOT_LOGGEDIN",
    user: "",
    fireRedirect: false,
  };
  onchangeHandler = (e) => {
    var name = e.target.name;
    var val = e.target.value;
    var updatedform = { ...this.state.formdata };
    var updatedElement = { ...updatedform[name] };
    updatedElement.value = val;
    updatedform[name] = updatedElement;
    this.setState({ formdata: updatedform });
    console.log(updatedElement);
  };
  loginHandler = (e) => {
      var login_data = {
          "username":this.state.formdata.email.value,
          "password":this.state.formdata.password.value
      }
    axios.post(`http://127.0.0.1:8000/login`,login_data).then(res=>{
        console.log("data is ",res.data)
        var result = res.data
        localStorage.setItem("username",result.username)
        localStorage.setItem("token",result.access_token)
    }).catch(error=>{
        console.log("errror occured",error)
    })

  };
  render() {
    return (
      <body className="hold-transition login-page">
        <div className="login-box">
          <div className="login-logo">
            <h1>Login</h1>
          </div>
          {/* /.login-logo */}
          <div className="card">
            <div className="card-body login-card-body">
              <p className="login-box-msg">Sign in to start your session</p>
              <form action="../../index3.html" method="post">
                <div className="input-group mb-3">
                  <input
                    type="email"
                    className="form-control"
                    placeholder="Email"
                    value={this.state.formdata.email.value}
                    onChange={this.onchangeHandler}
                    name="email"
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-envelope" />
                    </div>
                  </div>
                </div>
                <div className="input-group mb-3">
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Password"
                    value={this.state.formdata.password.value}
                    onChange={this.onchangeHandler}
                    name="password"
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-lock" />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-8">
                    <div className="icheck-primary">
                      <input type="checkbox" id="remember" />
                      <label htmlFor="remember">Remember Me</label>
                    </div>
                  </div>
                  {/* /.col */}
                  <div className="col-4">
                    <button
                      type="submit"
                      className="btn btn-primary btn-block"
                      onClick={this.loginHandler}
                    >
                      Sign In
                    </button>
                  </div>
                  {/* /.col */}
                </div>
              </form>
              <div className="social-auth-links text-center mb-3">
                <p>- OR -</p>
                <a href="#" className="btn btn-block btn-primary">
                  <i className="fab fa-facebook mr-2" /> Sign in using Facebook
                </a>
                <a href="#" className="btn btn-block btn-danger">
                  <i className="fab fa-google-plus mr-2" /> Sign in using
                  Google+
                </a>
              </div>
              {/* /.social-auth-links */}
              <p className="mb-1">
                <a href="forgot-password.html">I forgot my password</a>
              </p>
              <p className="mb-0">
                <a href="register.html" className="text-center">
                  Register a new membership
                </a>
              </p>
            </div>
            {/* /.login-card-body */}
          </div>
        </div>
      </body>
    );
  }
}
var mapDispatchToProps = (dispatch) => {
  return {
    onLogin: (email, password) => dispatch(actions.login(email, password)),
  };
};
var mapStateToProps = (state) => {
  return {
    loginId: state.log.userId,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Login);
